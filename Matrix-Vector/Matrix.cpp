//
//  Matrix.cpp
//  abc
//

#include "Matrix.h"

Matrix::Matrix(int rowNum, int colNum) {
    mat = new float* [rowNum];
    for (int i = 0; i < rowNum; ++i) {
        mat[i] = new float[colNum];
    }
    rows = rowNum;
    cols = colNum;
}

Matrix::Matrix(std::initializer_list<std::initializer_list<double>> listlist) {
    cols = (int)(listlist.begin())->size(); 
    rows = (int)listlist.size();

    mat = new float * [rows];

    for (int i = 0; i < rows; i++) {
        mat[i] = new float[cols];
        for (int j = 0; j < cols; j++) {
            mat[i][j] = ((listlist.begin() + i)->begin())[j];
        }
    }
}


Matrix::Matrix(const Matrix& temp) {
    rows = temp.rows;
    cols = temp.cols;

    mat = new float* [rows];

    for (int i = 0; i < rows; ++i) {
        mat[i] = new float[cols];

    }

    for (int i = 0; i < rows; ++i) {

        for (int j = 0; j < cols; ++j) {
            mat[i][j] = temp.mat[i][j];
        }

    }


}


Matrix Matrix::operator+(Matrix a) {

    if (a.rowSize() != this->rowSize() || a.rowSize() != this->colSize()) {
        std::cout << "Addition Not Possible, sizes are different" << std::endl;
        Matrix emptyMatrix = { {} };
        return emptyMatrix;
    }

    Matrix b(rowSize(), colSize());

    for (int i = 0; i < a.rowSize(); ++i) {
        for (int j = 0; j < a.colSize(); ++j) {
            b.mat[i][j] = this->mat[i][j] + a.mat[i][j];
        }
    }

    return b;
}

Matrix Matrix::operator-(Matrix a) {

    if (a.rowSize() != this->rowSize() || a.rowSize() != this->colSize()) {
        std::cout << "Addition Not Possible, sizes are different" << std::endl;
        Matrix emptyMatrix = { {} };
        return emptyMatrix;
    }

    Matrix b(rowSize(), colSize());

    for (int i = 0; i < a.rowSize(); ++i) {
        for (int j = 0; j < a.colSize(); ++j) {
            b.mat[i][j] = this->mat[i][j] - a.mat[i][j];
        }
    }

    return b;
}

Matrix Matrix::operator*(Matrix a) {

    if (this->colSize() != a.rowSize()) {
        std::cout << "Matrices Can not be multiplied. The column size of the first matrix is not equal to the row size of the second matrix" << std::endl;
        Matrix emptyMatrix = { {} };
        return emptyMatrix;
    }

    Matrix result(this->rowSize(), a.colSize());

    for (int row = 0; row < this->rowSize(); ++row) {
        for (int col = 0; col < a.colSize(); ++col) {
            result.mat[row][col] = 0;
            for (int i = 0;  i < a.rowSize();  i++)
            {
                result.mat[row][col] += this->mat[row][i] * a.mat[i][col];
            }
            
        }
    }
    return result;
}

Matrix Matrix::operator*(double a) {

    Matrix result(this->rowSize(), this->colSize());

    for (int row = 0; row < this->rowSize(); ++row) {
        for (int col = 0; col < this->colSize(); ++col) {
            result.mat[row][col] = a*this->mat[row][col];
        }
    }

    return result;
}


void Matrix::displayMatrix() {

    for (int i = 0; i < rowSize(); ++i) {
        for (int j = 0; j < colSize(); ++j) {

            std::cout << mat[i][j] << " ";

        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}







