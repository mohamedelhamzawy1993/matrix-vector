#include <stdio.h>
#include <iostream>
#include "Matrix.h"
#include "Vector.h"
int main()
{
    Matrix mat1 = { {1,2,3} ,{3,2,1}, {11,2,12} };
    Matrix mat2 = { {10,21,22} ,{13,4,21}, {12,11,10} };
    Matrix multiply =mat1*mat2;
    std::cout <<"Matrix multiplication result:" << std::endl;
    multiply.displayMatrix();
    std::cout << "Matrix Addition result:" << std::endl;
    Matrix add=mat1+mat2;
    add.displayMatrix();
    int random = 3;
    Matrix scaleMutiply(mat1 *(double)random);
    scaleMutiply.displayMatrix();
    Vector vec1 = { 1,2,4 };
    Vector vec2 = { 3,4,5 };
    Vector addVec = vec1 + vec2;
    std::cout << "Vector Addition result:" << std::endl;
    addVec.displayVector();
    Vector multiplyVec = vec1 * vec2;
    std::cout << "Vector Multiplication result:" << std::endl;
    multiplyVec.displayVector();
    std::cout << "Vector Scale Multiplication result:" << std::endl;
    Vector scaleMultVec = vec1 * 3;
    scaleMultVec.displayVector();

}
