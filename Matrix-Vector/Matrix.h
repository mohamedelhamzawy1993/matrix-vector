//
//  Matrix.h
//  abc
//
//  Created by hchoi on 4/5/15.
//  Copyright (c) 2015 hchoi. All rights reserved.
//


#include <stdio.h>
#include <iostream>

class Matrix {
private:
    int rows;
    int cols;
    float ** mat;

public:

    Matrix(int rowNum, int colNum);
    Matrix(std::initializer_list<std::initializer_list<double>> listlist);
    Matrix(const Matrix& temp);

    int rowSize() { return rows; }
    int colSize() { return cols; }
    float** matrix() { return mat; }

    void displayMatrix();
    Matrix operator+(Matrix b);

    Matrix operator-(Matrix a);

    Matrix operator*(Matrix a);

    Matrix operator*(double a);

};

//int main()
//{
//    Matrix X = { {3,2,4} ,{11,3,4} };
//    Matrix Y = { {10,21} ,{13,4}, {12,11} };
//    Matrix result(X*Y);
//    result.displayMatrix();
//    Matrix add(X + Y);
//    add.displayMatrix();
//    int random = 3;
//    Matrix scaleMutiply(X *(double)random);
//    scaleMutiply.displayMatrix();
//    X = X + Y;
//    
//    
//}


