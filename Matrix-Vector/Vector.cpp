#include "Vector.h"

Vector::Vector(int len)
{
	vec = new float[len];
	length = len;
	for (int i = 0; i < len; i++) {
		vec[i] = 0;
	}
}

Vector::Vector(std::initializer_list<float> list)
{
	length = list.size();
	vec = new float[length];
	for (int i = 0; i < length; i++)
	{
		vec[i] = list.begin()[i];
	}
}

Vector::Vector(const Vector& temp)
{
	this->length = temp.length;
	vec = new float[temp.length];
	for (int i = 0; i < length; i++)
	{
		vec[i] = temp.vec[i];
	}
}

Vector Vector::operator+(Vector other)
{
	if (this->length != other.length)
	{
		std::cout << "Additon is not possible. Vectors do not have the same length" << std::endl;
		Vector emptyVector = {};
		return emptyVector;
	}
	Vector result(other.length);
	for (int i = 0; i < length; i++)
	{
		result.vec[i] = this->vec[i] + other.vec[i];
	}
	return result;

}

Vector Vector::operator-(Vector other)
{
	if (this->length != other.length)
	{
		std::cout << "Additon is not possible. Vectors do not have the same length" << std::endl;
		Vector emptyVector = {};
		return emptyVector;
	}
	Vector result(length);
	for (int i = 0; i < length; i++)
	{
		result.vec[i] = this->vec[i] - other.vec[i];
	}
	return result;
}

Vector Vector::operator*(Vector other)
{
	if (this->length != other.length)
	{
		std::cout << "Multiplication is not possible. Vectors do not have the same length" << std::endl;
		Vector emptyVector = {};
		return emptyVector;
	}
	Vector result(length);
	for (int i = 0; i < length; i++)
	{
		result.vec[i] = this->vec[i] * other.vec[i];
	}
	return result;

}

//double Vector::operator*(Matrix mat)
//{
//	if (mat.colSize() != 1) {
//		std::cout << "Multiplication is not possible. Matrix must be a row matrix " << std::endl;
//		double empty;
//		return empty;
//	}
//
//	double result=0;
//	for (int i = 0; i < length; i++)
//	{
//		result += this->vec[i] * mat.matrix()[i][0];
//	}
//	return result;
//}

Vector Vector::operator*(float num)
{

	Vector result(length);
	for (int i = 0; i < length; i++)
	{
		result.vec[i] = this->vec[i] * num;
	}
	return result;
}

void Vector::displayVector()
{
	for (int j = 0; j < length; ++j) {

		std::cout << this->vec[j] << std::endl;
		
	}
	std::cout << std::endl;
}
