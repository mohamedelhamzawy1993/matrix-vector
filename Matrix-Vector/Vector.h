/// <summary>
/// Vector.h
/// </summary>


#include <stdio.h>
#include <iostream>



class Vector
{
private:

	int length;
	float* vec;

public:
	Vector(int length);
	Vector(std::initializer_list<float> list);
	Vector(const Vector& temp);

	Vector operator+(Vector other);
	Vector operator-(Vector other);
	Vector operator*(Vector other);
	//double operator*(Matrix num);
	Vector operator*(float num);

	void displayVector();
	


};



